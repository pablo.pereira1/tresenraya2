package tresenraya2;

public class MainTresEnRaya {

	private final char J1 = 'X';
	private final char J2 = 'O';
	private final char VACIO = '-';
	private boolean turno;
	private char tablero[][];

	public MainTresEnRaya() {
		this.turno = true;
		this.tablero = new char[3][3];
		this.inicializarTablero();
	}

	private void inicializarTablero() {

		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				tablero[i][j] = VACIO;
			}
		}

	}
	
	public void mostrarTablero() {
		 
	    for (int i = 0; i < this.tablero.length; i++) {
	        for (int j = 0; j < this.tablero[0].length; j++) {
	            System.out.print(this.tablero[i][j] + " ");
	        }
	        System.out.println("");
	    }
	 
	}
	public boolean posicionValida(int fila,int columna) {
		if(this.tablero[fila][columna]!=VACIO) {
			return true;
		}
		return false;
	}
	
	public void mostrarTurnoActual() {
	 
	    if (turno) {
	        System.out.println("Le toca al jugador 1");
	    } else {
	        System.out.println("Le toca al jugador 2");
	    }
	 
	}
	 
	public void cambiaTurno() {
	    this.turno = !this.turno;
	}
	public void colocarFicha(int fila, int columna) {
	    if (turno) {
	        this.tablero[fila][columna] = J1;
	    } else {
	        this.tablero[fila][columna] = J2;
	    }
	}

	private char hayGanador() {
	 
	    char simbolo;
	    boolean coincidencia;
	 
	    for (int i = 0; i < tablero.length; i++) {
	 
	        //Reiniciamos la coincidencia
	        coincidencia = true;
	        //Cogemos el simbolo de la fila
	        simbolo = tablero[i][0];
	        if (simbolo != VACIO) {
	            for (int j = 1; j < tablero[0].length; j++) {
	                //sino coincide ya no habra ganadro en esta fila
	                if (simbolo != tablero[i][j]) {
	                    coincidencia = false;
	                }
	            }
	 
	            //Si no se mete en el if, devuelvo el simbolo ganador
	            if (coincidencia) {
	                return simbolo;
	            }
	 
	        }
	 
	    }
	 
	    //Si no hay ganador, devuelvo el simbolo por defecto
	    return VACIO;
	 
	}
}
